import requests

from SkedClasses.SkedBase import SkedBase


class SkedData(SkedBase):
    """
    Data object
    """
    def __init__(self, subject_id, subject_name, experiment_id, data_type):
        SkedBase.__init__(self, subject_id, subject_id, subject_id, dict())
        self.internal_id = subject_id
        self.name = subject_name
        self.experiment_id = experiment_id
        self.data_type = data_type
        self.data_associations = self.get_data_associations()

    def get_data_associations(self):
        headers = {'Authorization': 'Bearer ' + self.auth_token}
        payload = {'experiment_id': self.experiment_id, 'subject_id': self.internal_id, 'data_type': self.data_type}
        resp = requests.get(self.api + 'protocols', headers=headers, params=payload)
        return resp.json()
