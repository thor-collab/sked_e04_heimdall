class SkedTimeSeries:
    """
    Time series object
    """
    def __init__(self, time, var_names, table):
        self.type = 'TimeSeries'
        self.time = time
        self.var_names = var_names
        self.table = table
