import requests

from SkedClasses.SkedBase import SkedBase
from SkedClasses.SkedData import SkedData


class SkedSubject(SkedBase):
    """
    Subject object
    """
    def __init__(self, subject_id, experiment_id):
        SkedBase.__init__(self, subject_id, subject_id, subject_id, dict())
        self.experiment_id = experiment_id
        subject = self.get_subject()
        subject_name = subject['name']
        self.name = subject_name
        self.external_id = subject_name

        # Create the SkedData objects
        protocols = self.get_metadata()
        data_types = [protocol['data_type'] for protocol in protocols]
        self.metadata = protocols
        self.data_types = set(data_types)
        self.data = {}
        for data_type in self.data_types:
            self.data[data_type] = SkedData(self.internal_id, self.name, self.experiment_id, data_type)

    def get_metadata(self):
        headers = {'Authorization': 'Bearer ' + self.auth_token}
        payload = {'experiment_id': self.experiment_id, 'subject_id': self.internal_id}
        resp = requests.get(self.api + 'protocols', headers=headers, params=payload)
        return resp.json()

    def get_subject(self):
        headers = {'Authorization': 'Bearer ' + self.auth_token}
        params = {'subject_id': self.internal_id}
        resp = requests.get(self.api + 'subjects', headers=headers, params=params)
        return resp.json()[0]  # Return only subject from call
