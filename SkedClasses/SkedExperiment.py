import requests

from SkedClasses.SkedBase import SkedBase
from SkedClasses.SkedSubject import SkedSubject


class SkedExperiment(SkedBase):
    """
    Experiment object
    """
    def __init__(self, name):
        SkedBase.__init__(self, name, name, name, dict())
        experiment_data = self.get_experiment()
        self.description = experiment_data['description']
        subject_ids = self.get_subject_ids()
        self.subjects = [SkedSubject(subject_id, self.internal_id) for idx, subject_id in enumerate(subject_ids)]

    def get_experiment(self):
        headers = {'Authorization': 'Bearer ' + self.auth_token}
        params = {'id': self.name}
        resp = requests.get(self.api + 'experiments', headers=headers, params=params)
        return resp.json()[0]  # Return the first and only experiment

    def get_subject_ids(self):
        headers = {'Authorization': 'Bearer ' + self.auth_token}
        params = {'experiment_id': self.internal_id}
        resp = requests.get(self.api + 'subjects', headers=headers, params=params)
        subjects = resp.json()
        return [subject['subject_id'] for subject in subjects]
