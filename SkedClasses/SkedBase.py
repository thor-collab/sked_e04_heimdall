import configparser
import requests


class SkedBase(object):
    """
    Base class that other SKED classes inherits from
    """
    def __init__(self, name, internal_id, external_id, metadata, api='http://10.114.119.52/api/'):
        self.name = name
        self.internal_id = internal_id
        self.external_id = external_id
        self.metadata = metadata
        self.api = api
        self.auth_token = self.get_token()

    def get_token(self):
        # Read the credentials provided in credentials.ini
        config = configparser.ConfigParser()
        config.read('credentials.ini')

        # Make the call to get the authorization token
        credentials = {'username': config['aws']['username'], 'password': config['aws']['password']}
        resp = requests.post(self.api + 'auth/login', json=credentials)
        auth_token = resp.content.decode("utf-8")
        return auth_token
